package cn.backflow.admin.service;

import cn.backflow.admin.Constants;
import cn.backflow.admin.entity.Permission;
import cn.backflow.admin.entity.Role;
import cn.backflow.admin.entity.RolePermission;
import cn.backflow.admin.repository.PermissionRepository;
import cn.backflow.admin.repository.RoleRepository;
import cn.backflow.data.service.AbstractService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class RoleService extends AbstractService<Role, Integer> {

    private final PermissionRepository permissionRepo;
    private final RoleRepository roleRepo;

    @Autowired
    public RoleService(PermissionRepository permissionRepo, RoleRepository roleRepo) {
        this.permissionRepo = permissionRepo;
        this.roleRepo = roleRepo;
    }

    public List<Integer> findOwnedPermissionId(Object parameter) {
        return roleRepo.findPermissionId(parameter);
    }

    public List<String> findOwnedPermission(Object parameter) {
        return roleRepo.findPermission(parameter);
    }

    @Transactional
    protected void deleteRolePermissionsByRoleId(Integer roleId) {
        roleRepo.deleteRolePermissions(Collections.singletonMap("roleId", roleId));
    }

    /**
     * 保存角色权限
     *
     * @param id    角色ID
     * @param perms 权限ID集合
     */
    @Transactional
    @CacheEvict(value = Constants.PERMISSION_CACHE, allEntries = true)
    public void saveRolePermissions(Integer id, Integer[] perms) {
        deleteRolePermissionsByRoleId(id);
        if (perms == null || perms.length == 0) {
            return;
        }
        List<Permission> permissions = permissionRepo.findAll(Collections.singletonMap("ids", perms));
        if (permissions.isEmpty()) {
            return;
        }
        List<RolePermission> rolePermissions = permissions.stream()
                .map(p -> new RolePermission(id, p.getId(), p.getCode()))
                .collect(Collectors.toList());
        roleRepo.insertRolePermission(rolePermissions);
    }

    @Transactional
    @CacheEvict(value = Constants.PERMISSION_CACHE, allEntries = true)
    public int persistWithPermission(Role role, Set<Integer> perms) {
        // 保存角色
        int effected = role.getId() == null ? save(role) : update(role);
        // 清空角色权限
        deleteRolePermissionsByRoleId(role.getId());

        if (!perms.isEmpty()) {
            List<Permission> permissions = permissionRepo.findByIds(perms); // 根据ID获取权限标识
            List<RolePermission> rps = permissions.stream()
                    .map(p -> new RolePermission(role.getId(), p.getId(), p.getCode()))
                    .collect(Collectors.toList());

            // 保存角色权限
            roleRepo.insertRolePermission(rps);
        }
        return effected;
    }

    @Deprecated /* 已弃用, 用户只与单个角色关联 */
    @Transactional
    @CacheEvict(value = Constants.PERMISSION_CACHE, allEntries = true)
    public void saveUserRoles(Integer userId, Integer[] roles) {
        roleRepo.deleteUserRoles(userId);
        roleRepo.saveUserRoles(userId, roles);
    }

    public List<Role> findUserRoles(Integer id) {
        return roleRepo.findAll(Collections.singletonMap("userId", id));
    }
}