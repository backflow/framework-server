package cn.backflow.admin.service;

import cn.backflow.admin.entity.Department;
import cn.backflow.admin.repository.DepartmentRepository;
import cn.backflow.admin.repository.UserRepository;
import cn.backflow.data.service.AbstractService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class DepartmentService extends AbstractService<Department, Integer> {

    private final DepartmentRepository departmentRepo;
    private final UserRepository userRepo;

    @Autowired
    public DepartmentService(DepartmentRepository departmentRepo, UserRepository userRepo) {
        this.departmentRepo = departmentRepo;
        this.userRepo = userRepo;
    }

    @Override
    public List<Department> findAll(Object parameter) throws DataAccessException {
        return super.findAll(parameter);
    }

    public Map<Comparable, Department> findMap(Object filters) {
        return departmentRepo.findMap(filters, "id");
    }

    /**
     * 查找兄弟部门
     *
     * @param id             部门ID
     * @param excludeCurrent 是否排除当前部门
     * @return 所有同层级的部门列表
     */
    public List<Department> findSiblings(Integer id, boolean excludeCurrent) {
        return departmentRepo.findSiblings(id, excludeCurrent);
    }

    /**
     * 查找子元素
     *
     * @param parent 父级部门ID
     * @param direct 是否只查找直接子部门
     * @return 该部门的直接子部门（<code>direct</code>为<code>true</code>是返回不限层级的所有子部门）
     */
    public List<Department> findByParent(Integer parent, boolean direct) {
        Map<String, Object> parameter = new HashMap<>();
        parameter.put("parent", parent);
        parameter.put("direct", direct);
        return departmentRepo.findAll(parameter);
    }

    /**
     * 更新部门排序
     *
     * @param id   部门ID
     * @param from 起始位置
     * @param to   结束位置
     * @return 修改影响记录数
     */
    @Transactional
    public int updateSeq(int id, int from, int to) {
        List<Department> siblings = findSiblings(id, false);
        for (int i = 0, len = siblings.size(); i < len; i++) {
            Department d = siblings.get(i);
            d.setSeq(i);
            if (i == from) {
                d.setSeq(to);
            }
            if (i == to) {
                d.setSeq(from);
            }
        }
        // TODO 优化: 通过UPDATE单个seq字段更新
        return departmentRepo.updateSelectiveBatch(siblings);
    }

    @Override
    public int save(Department entity) throws DataAccessException {
        prepareForSaveOrUpdate(entity);
        return super.update(entity);
    }

    @Override
    public int update(Department entity) throws DataAccessException {
        prepareForSaveOrUpdate(entity);
        return super.update(entity);
    }

    @Override
    public int saveOrUpdate(Department entity) throws DataAccessException {
        prepareForSaveOrUpdate(entity);
        return super.saveOrUpdate(entity);
    }

    @Override
    public int deleteById(Integer id) throws DataAccessException {
        userRepo.separateDepartment(id);
        return super.deleteById(id);
    }

    /**
     * 设置部门层级与祖先路径
     */
    private void prepareForSaveOrUpdate(Department department) {
        if (department.getId() == null)
            departmentRepo.insert(department);
        int level = 1;
        String ancestors = department.getId().toString();
        if (department.getParent() != null && department.getParent() != 0) {
            Department parent = getById(department.getParent());
            level = parent.getLevel() + 1;
            ancestors = parent.getAncestors() + "," + ancestors;
        }
        department.setLevel(level);
        department.setAncestors(ancestors);
    }
}