package cn.backflow.admin.repository;

import cn.backflow.admin.entity.Department;
import cn.backflow.data.pagination.Page;
import cn.backflow.data.pagination.PageRequest;
import cn.backflow.data.repository.BaseMyBatisRepository;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
public class DepartmentRepository extends BaseMyBatisRepository<Department, Integer> {


    @Override
    public int saveOrUpdate(Department department) {
        return department.getId() == null ? insert(department) : update(department);
    }

    public Page<Department> findByPageRequest(PageRequest pageRequest) {
        return pageQuery("Department.paging", pageRequest);
    }

    public List<Department> findSiblings(Integer id, boolean excludeCurrent) {
        Map<String, Object> parameter = new HashMap<>();
        parameter.put("excludeCurrent", excludeCurrent);
        parameter.put("id", id);
        return sqlSession.selectList("Department.findSiblings", parameter);
    }
}