package cn.backflow.admin;

public class Constants {
    public static final int USER_STATUS_LOCKED = 0; // 用户锁定状态

    public static final String SYSTEM_CACHE = "admin";
    public static final String PERMISSION_CACHE = "permissions";

    public static final String WORDS_SPLITER = "、|，|,|\\s+";

    public static String LOGIN_FAILURE_MAP = "login_failure_map";
    public static String USER_SCREEN_LOCKED = "user_screen_locked";

    public static String KAPTCHA_SESSION_KEY = "kaptcha_session_key"; // Session中保存验证码的key
    public static String SESSION_PERMISSIONS_KEY = "permissions"; // Session中保存用户登陆信息的key
    public static String SESSION_USER_KEY = "user"; // Session中保存用户登陆信息的key
}