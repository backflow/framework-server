package cn.backflow.integration.redis;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.RedisClusterConfiguration;
import org.springframework.data.redis.connection.RedisClusterNode;
import org.springframework.data.redis.connection.RedisPassword;
import org.springframework.data.redis.connection.RedisStandaloneConfiguration;
import org.springframework.data.redis.connection.lettuce.LettuceConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;

@Configuration
public class RedisConfiguration {

    @Bean
    public LettuceConnectionFactory redisConnectionFactory() {
        return new LettuceConnectionFactory(standaloneConfiguration());
    }

    @Bean
    public RedisTemplate<?, ?> redisTemplate() {
        return new RedisTemplate<byte[], byte[]>();
    }

    private RedisStandaloneConfiguration standaloneConfiguration() {
        RedisStandaloneConfiguration standaloneConfiguration = new RedisStandaloneConfiguration("47.75.48.102", 6379);
        standaloneConfiguration.setPassword(RedisPassword.of("taeyeon"));
        return standaloneConfiguration;
    }


    private RedisClusterConfiguration clusterConfiguration() {
        RedisClusterConfiguration redisClusterConfiguration = new RedisClusterConfiguration();

        redisClusterConfiguration.addClusterNode(new RedisClusterNode("localhost", 6379));
        redisClusterConfiguration.addClusterNode(new RedisClusterNode("localhost", 6379));

        return redisClusterConfiguration;
    }
}
